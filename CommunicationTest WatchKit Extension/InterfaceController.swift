//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {

    
    // MARK: Outlets
    // ---------------------
   
    @IBOutlet var btnFeedOutlet: WKInterfaceButton!
    @IBOutlet var btnNameOutlet: WKInterfaceButton!
    @IBOutlet var btnStartOutlet: WKInterfaceButton!
    @IBOutlet var btnHibernateOutlet: WKInterfaceButton!
    
    @IBOutlet var btnGiveNameOutlet: WKInterfaceButton!
    var pokemonName:String!
    var pokemonType:String!
    var health = 100
    var hunger = 0
    var pokemonState:String!
    var count = 1
    var updateSpeed = 1
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // Label for other messages (HP:100, Hunger:0)
    @IBOutlet var outputLabel: WKInterfaceLabel!
    
    
    
    // MARK: Delegate functions
    // ---------------------

    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        // Message from phone comes in this format: ["course":"MADT"]
        if (self.count == 1){
        let messageBody = message["pokemon"] as! String
        //messageLabel.setText(messageBody)
        self.pokemonType = messageBody
        print("WATCH: Got message from Phone pkemon = \(messageBody)")
        self.btnGiveNameOutlet.setHidden(false)
        self.btnGiveNameOutlet.setTitle("Give me a name")
        self.pokemonImageView.setHidden(false)
        if (messageBody == "pikachu"){
            self.pokemonImageView.setImageNamed("pikachu")
        }
        else if (messageBody == "caterpie"){
            self.pokemonImageView.setImageNamed("caterpie")
        }
        }
        if (self.count >= 2){
            print("recieved baby state back from phone")
             print(message)
            
            self.pokemonType = message["pokemonType"] as! String
            self.pokemonName = message["pokemonName"] as! String
            self.health = message["pokemonHealth"] as! Int
            self.hunger = message["pokemonHunger"] as! Int
            
            self.pokemonState = "wake"
            self.btnHibernateOutlet.setHidden(false)
            self.btnFeedOutlet.setHidden(false)
            self.callback()
            
        }
        self.count = self.count + 1
    }
    


    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        //self.callback()
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    // ---------------------
    
    
    // MARK: Functions for Pokemon Parenting
    @IBAction func btnGiveMeName() {
        
        let suggestedResponses = ["Albert", "Pritesh", "Jenelle"]
        
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) {
            
            (results) in
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.nameLabel.setHidden(false)
                self.btnStartOutlet.setHidden(false)
                self.nameLabel.setText("My Name is \(userResponse!)")
                
                // 3. also save the user's choice to the cityName variable
                self.pokemonName = userResponse
            }
        }

    }
    @IBAction func nameButtonPressed() {
        print("name button pressed")
    }

    @IBAction func startButtonPressed() {
        self.callback()
        print("Start button pressed")
        self.btnFeedOutlet.setHidden(false)
        self.btnHibernateOutlet.setHidden(false)
        self.outputLabel.setHidden(false)
    
        self.btnStartOutlet.setHidden(true)
        
    }
    
    @IBAction func feedButtonPressed() {
        self.hunger = self.hunger - 12
        print("Feeded, hunger reduced to \(self.hunger)")
    }
    
    @IBAction func hibernateButtonPressed() {
        print("Hibernate button pressed")
        self.pokemonState = "sleeping"
        
        if (WCSession.default.isReachable) {
        print("phone reachable")
            let message = ["pokemonType": self.pokemonType,"pokemonName":self.pokemonName,"health":self.health,"hunger":self.hunger] as [String : Any]
                WCSession.default.sendMessage(message, replyHandler: nil)
                // output a debug message to the console
                print("sent baby state to phone")
                self.btnFeedOutlet.setHidden(true)
                self.btnHibernateOutlet.setHidden(true)
            

        }
        else {
            print("WATCH: Cannot reach phone")
        
        }
        
        
    }
    @objc func callback() {
        if (pokemonState != "sleeping"){
        //self.hunger = self.hunger + 10
            
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            
                self.callback()
            self.hunger = self.hunger + 10
            print("HP: \(self.health)   Hunger: \(self.hunger)")
            
        
        }
        self.outputLabel.setText("HP: \(self.health)   Hunger: \(self.hunger)")
        if (self.hunger >= 80)&&(self.health > 0){
            self.health = self.health - 5
            self.outputLabel.setText("HP: \(self.health)   Hunger: \(self.hunger)")
        }
        if (self.health <= 0){
            self.pokemonState = "dead"
        
        }
            if(self.hunger <= 10){
            self.nameLabel.setText("\(self.pokemonName!) is not Hungry")
            }
            if(self.hunger > 10)&&(self.hunger <= 50){
                self.nameLabel.setText("\(self.pokemonName!) is Hungry")
            }
            if(self.hunger > 50)&&(self.hunger <= 80){
                self.nameLabel.setText("\(self.pokemonName!) is so Hungry")
            }
            if(self.health <= 0){
                self.nameLabel.setText("\(self.pokemonName!) is Dead")
                self.hunger = 0
            }
        }
    }
}
