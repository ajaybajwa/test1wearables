//
//  ViewController.swift
//  CommunicationTest
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate  {

    
    var babySelected:String!
    // MARK: Outlets
    @IBOutlet weak var outputLabel: UITextView!
    
    @IBOutlet weak var btnSendOutlet: UIButton!
    @IBOutlet weak var lblPokemonState: UILabel!
    @IBOutlet weak var btnwakeUpOutlet: UIButton!
    // MARK: Required WCSessionDelegate variables
    // ------------------------------------------
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        //@TODO
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        //@TODO
    }
    
    // MARK: Receive messages from Watch
    // -----------------------------------
    
    // 3. This function is called when Phone receives message from Watch
    //func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]){
        
        // 1. When a message is received from the watch, output a message to the UI
        // NOTE: Since session() runs in background, you cannot directly update UI from the background thread.
        // Therefore, you need to wrap any UI updates inside a DispatchQueue for it to work properly.
        
        DispatchQueue.main.async {
            self.outputLabel.insertText("\nMessage Received: \(message)")
            let type = message["pokemonType"] as! String
            let name = message["pokemonName"] as! String
            let health = message["health"] as! Int
            let hunger = message["hunger"] as! Int
//
//            print("Name: \(name)")
//            print("Type: \(type)")
//            print("Health: \(health)")
//            print("Hunger: \(hunger)")
//
            // save the city to shared preferences
            let sharedPreferences = UserDefaults.standard
            sharedPreferences.set(type, forKey:"pokemonType")
            sharedPreferences.set(name, forKey:"pokemonName")
            sharedPreferences.set(health, forKey:"pokemonHealth")
            sharedPreferences.set(hunger, forKey:"pokemonHunger")
            self.outputLabel.insertText("\nSaved \(name) to shared preferences!")
            
            self.lblPokemonState.isHidden = false
            self.lblPokemonState.text = "\(name) is Hibernating"
            self.btnwakeUpOutlet.isHidden = false
        
            
        }
        
        // 2. Also, print a debug message to the phone console
        // To make the debug message appear, see Moodle instructions
        print("Received a message from the watch: \(message)")
    }

    
    // MARK: Default ViewController functions
    // -----------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        DispatchQueue.main.async {
        // 1. Check if phone supports WCSessions
        print("view loaded")
        if WCSession.isSupported() {
            self.outputLabel.insertText("\nPhone supports WCSession")
            WCSession.default.delegate = self
            WCSession.default.activate()
            self.outputLabel.insertText("\nSession activated")
        }
        else {
            print("Phone does not support WCSession")
            self.outputLabel.insertText("\nPhone does not support WCSession")
        }
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: Actions
    // -------------------
    @IBAction func sendMessageButtonPressed(_ sender: Any) {
        
        // 2. When person presses button, send message to watch
        outputLabel.insertText("\nTrying to send message to watch")
        
        // 1. Try to send a message to the phone
        if (WCSession.default.isReachable) {
            if (self.babySelected == nil){
                print("Please select a baby")
                outputLabel.insertText("\nPlease select a baby")
            }
            else{
            let message = ["pokemon": self.babySelected!]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            outputLabel.insertText("\nMessage sent to watch")
            // output a debug message to the console
            print("Message sent to watch")
            self.btnSendOutlet.isHidden = true
            }
        }
        else {
            print("PHONE: Cannot reach watch")
            outputLabel.insertText("\nCannot reach watch")
        }
    }
    
    
    // MARK: Choose a Pokemon actions
    
    @IBAction func pokemonButtonPressed(_ sender: Any) {
        self.babySelected = "pikachu"
        print("You pressed the \(self.babySelected!) button")
        
    }
    @IBAction func caterpieButtonPressed(_ sender: Any) {
        self.babySelected = "caterpie"
        print("You pressed the \(self.babySelected!) button")
    }
    
    @IBAction func btnWakeUp(_ sender: Any) {
        let sharedPreferences = UserDefaults.standard
        let poktype = sharedPreferences.string(forKey: "pokemonType")
        let pokname = sharedPreferences.string(forKey: "pokemonName")
        let pokhealth = sharedPreferences.integer(forKey: "pokemonHealth")
        let pokhunger = sharedPreferences.integer(forKey: "pokemonHunger")
        
        if (WCSession.default.isReachable) {
            print("Watch reachable")
            let message = ["pokemonType": poktype,"pokemonName":pokname,"pokemonHealth":pokhealth,"pokemonHunger":pokhunger] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the console
            outputLabel.insertText("sent baby state back to watch")
            self.lblPokemonState.text = "No one is Hibernating"
            
            
            
        }
        else {
            outputLabel.insertText("WATCH: Cannot reach watch")
            
        }

    }
    
}

